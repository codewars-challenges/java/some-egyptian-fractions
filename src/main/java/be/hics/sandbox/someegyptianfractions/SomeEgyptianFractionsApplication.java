package be.hics.sandbox.someegyptianfractions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SomeEgyptianFractionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SomeEgyptianFractionsApplication.class, args);
        System.out.println(Decomp.decompose("2", "8"));
        System.out.println(Decomp.decompose("125", "100"));
        System.out.println(Decomp.decompose("21", "23"));
        System.out.println(Decomp.decompose("3", "4"));
        System.out.println(Decomp.decompose("12", "4"));
        System.out.println(Decomp.decompose("0", "2"));
        System.out.println(Decomp.decompose("9", "10"));
	}
}
