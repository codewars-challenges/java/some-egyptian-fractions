package be.hics.sandbox.someegyptianfractions;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Decomp {

    public static String decompose(String nrStr, String drStr) {
        List<BigInteger> fraction = simplify(new BigInteger(nrStr), new BigInteger(drStr));
        return decompose(fraction.get(0), fraction.get(1));
    }

    private static String decompose(final BigInteger nr, final BigInteger dr) {
        List<String> decomposition = new ArrayList<>();
        List<List<BigInteger>> nextFractions;
        BigInteger naturalNumber = nr.divide(dr);
        if (naturalNumber.compareTo(BigInteger.ZERO) > 0) {
            decomposition.add(naturalNumber.toString());
            List<BigInteger> rest = simplify(nr.mod(dr), dr);
            nextFractions = calculateNext(rest.get(0), rest.get(1));
        } else {
            nextFractions = calculateNext(nr, dr);
        }
        if (Objects.nonNull(nextFractions) && nextFractions.size() > 0) {
            decomposition.add(String.format("1/%s", nextFractions.get(0).get(1).toString()));
            if (nextFractions.size() > 1) {
                while (nextFractions.get(1).get(0).compareTo(BigInteger.ONE) > 0) {
                    nextFractions = calculateNext(nextFractions.get(1).get(0), nextFractions.get(1).get(1));
                    decomposition.add(String.format("1/%s", Objects.requireNonNull(Objects.requireNonNull(nextFractions).get(0)).get(1).toString()));
                }
                decomposition.add(String.format("1/%s", nextFractions.get(1).get(1).toString()));
            }
        }
        return String.format("[%s]", decomposition.stream().collect(Collectors.joining(", ")));
    }

    private static List<List<BigInteger>> calculateNext(final BigInteger nr, final BigInteger dr) {
        if (nr.compareTo(BigInteger.ZERO) == 0 || dr.compareTo(BigInteger.ZERO) == 0)
            return null;
        if (nr.compareTo(BigInteger.ONE) == 0)
            return Stream.of(toFraction(nr, dr)).filter(Objects::nonNull).collect(Collectors.toList());
        BigInteger baseDr = calculateBaseDenominator(nr, dr);
        List<BigInteger> baseFraction = toFraction(BigInteger.ONE, baseDr);
        List<BigInteger> restFraction = calculateRestFraction(nr, dr, baseDr);
        return Stream.of(baseFraction, restFraction).filter(Objects::nonNull).collect(Collectors.toList());
    }

    private static BigInteger calculateBaseDenominator(final BigInteger nr, final BigInteger dr) {
        BigDecimal nrDec = new BigDecimal(nr);
        BigDecimal drDec = new BigDecimal(dr);
        BigDecimal ceiling = (drDec).divide(nrDec, RoundingMode.CEILING);
        return ceiling.toBigInteger();
    }

    private static List<BigInteger> calculateRestFraction(final BigInteger nr, final BigInteger dr, final BigInteger baseDr) {
        return simplify(baseDr.multiply(nr).subtract(dr), baseDr.multiply(dr));
    }

    private static List<BigInteger> toFraction(final BigInteger nr, final BigInteger dr) {
        return Stream.of(nr, dr).collect(Collectors.toList());
    }

    private static List<BigInteger> simplify(final BigInteger nr, final BigInteger dr) {
        BigInteger gcd = nr.gcd(dr);
        if (gcd.compareTo(BigInteger.ONE) > 0) {
            return toFraction(nr.divide(gcd), dr.divide(gcd));
        } else {
            return toFraction(nr, dr);
        }
    }

}